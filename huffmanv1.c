/*References: https://engineering.purdue.edu/ece264/17au/hw/HW13?alt=huffman
============================================================================
AUTHOR: JUBRIL GBOLAHAN ADIGUN
INSTITUTE: TALLINN UNIVESRSITY OF TECHNOLOGY
COURSE: SYSTEM PROGRAMMING
ASSIGNMENT: HUFFMAN ENCODER
FALL 2019
============================
*/

//==========================
//Including libraries
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
//==========================

//==========================
//function declarations
int read_header(FILE *f);
int write_header(FILE *f);
int read_bit(FILE *f);
int write_bit(FILE *f, int bit);
int flush_buffer(FILE *f);
void decode_bit_stream(FILE *fin, FILE *fout);
int decode(const char* ifile, const char *ofile);
void encode_alphabet(FILE *fout, int character);
int encode(const char* ifile, const char *ofile);
void build_tree();
void add_leaves();
int add_node(int index, int weight);
void init();
void finalise();
//=========================================================


//data structure type (ASCII) i.e 8bits, 2^8 = 256
int max_str = 256; //num_alphabets
int data_count = 0;  /* number of characters seen //num_active */ 
unsigned int size_data = 0; //number of data stream //original_size

int freq[256]={0}; //
int i; //global

//int buff[max_str] = {0};

void readfile_freq(){
	
    FILE *locfile;    /* pointer for input file */
   /* character or EOF flag from input */

    locfile = fopen("input.txt", "r");
    if (locfile == NULL) {
        printf("Cannot open the file\n");
        exit(1);
    }
    
//printf("Hello World\n");
    while((i = fgetc(locfile))!=EOF && (i != 10)) {
    	//printf("Hello World\n");
        ++freq[i];
        ++size_data;
//        if (ch == EOF)
    }
    
	for(i=0;i<max_str;i++){
    	if(freq[i] > 0) {
    		printf("Element %c appeared %d times\n",i, freq[i]);
    		++data_count;
		}
	}
	
	printf("Number of unique elements in the data stream %d from %d", data_count, size_data);
    fclose(locfile);
}

// assigning weights to nodes of binary tree
typedef struct {
	int index;
	unsigned int weight;
} 
node_t;

node_t *nodes = NULL;
int num_nodes = 0;
int *leaf_index = NULL;
int *parent_index = NULL;

void init() {
	freq = (int *) calloc(2 * max_str, sizeof(int));
//	freq[256] =(2 * max_str, sizeof(int));
	leaf_index = sizeof(freq) + (max_str) - 1;
}

void allocate_tree() {
	nodes = (node_t *)calloc(2 * data_count, sizeof(node_t));
	parent_index = (int *) calloc(data_count, sizeof(int));
}

void finalise() {
	free(parent_index);
//	free(freq);
	free(nodes);
}


int add_node(int index, int weight) {
//	int 
	i = num_nodes++;
	//Move existing nodes with larger weights to the right and
	//Add new node to its rightful place
	while (i > 0 && nodes[i].weight > weight) {
		memcpy(&nodes[i + 1], &nodes[i], sizeof(node_t));
			if (nodes[i].index < 0)
				++leaf_index[-nodes[i].index];
		
			else
				++parent_index[nodes[i].index];
				--i;
	}

//add node in the rightful place
//============================================
			++i;
			nodes[i].index = index;
			nodes[i].weight = weight;
			if (index < 0)
			leaf_index[-index] = i;
			else
			parent_index[index] = i;
//========================================
	
	return i;
}

void add_leaves() {
	int frequency;
	
	for (i = 0; i < max_str; ++i) {
		frequency = freq[i];
		if (frequency > 0)
		add_node(-(i + 1), frequency);
	}
}




//main program
int main(){
	readfile_freq();
	init();
	allocate_tree();
	return 0;
}
