/*References: https://engineering.purdue.edu/ece264/17au/hw/HW13?alt=huffman
============================================================================
AUTHOR: JUBRIL GBOLAHAN ADIGUN
INSTITUTE: TALLINN UNIVESRSITY OF TECHNOLOGY
COURSE: SYSTEM PROGRAMMING
ASSIGNMENT: HUFFMAN ENCODER
FALL 2019
============================
*/

//==========================
//Including libraries
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
//==========================

//==========================
//function declarations
int read_header(FILE *f);
int write_header(FILE *f);
int read_bit(FILE *f);
int write_bit(FILE *f, int bit);
int flush_buffer(FILE *f);
void decode_bit_stream(FILE *fin, FILE *fout);
int decode(FILE *fin, FILE *fout);
//void encode_character(FILE *fout, int character);
int encode(FILE *fin, FILE *fout);
void build_tree();
void add_leaves();
int add_node(int index, int weight);
void init();
void finalise();
//=========================================================

/*global variables
=======================================================================
//data structure type (ASCII) i.e 0 to 255 type character, 2^8 = 256*/
int max_str = 256; //num_alphabets
int data_count = 0;  /* number of characters seen //num_active */ 
unsigned int size_data = 0; //number of data stream //original_size

int freq[256]={0}; //
int *frequency = freq;
int i, *stack, stack_top, free_index = 1; 

unsigned char buffer[256];
int buffer_bits = 0;
int current_bit = 0;


//int buff[max_str] = {0};

void readfile_freq(FILE *fin){
   /* pointer for input file */
   /* character or EOF flag from input */
    while((i = fgetc(fin))!=EOF && (i != 10)) {
    	//printf("Hello World\n");
        ++freq[i];
        ++size_data;
//        if (ch == EOF)
    }
    
	for(i=0;i<max_str;i++){
    	if(freq[i] > 0) {
    		printf("Element %c appeared %d times\n",i, freq[i]);
    		++data_count;
		}
	}
	
	printf("Number of unique elements in the data stream %d from %d", data_count, size_data);
    fclose(fin);
}

// assigning weights to nodes of binary tree
typedef struct {
	int index;
	unsigned int weight;
} 
node_t;

node_t *nodes = NULL;
int total_nodes = 0;
int *leaf_index = NULL;
int *top_index = NULL;

//void init() {
//	//freq[256] = (int *) calloc(2 * max_str, sizeof(int));
////	freq[256] =(2 * max_str, sizeof(int));
//	leaf_index = frequency + max_str - 1;
//}

//void allocate_tree() {
//	nodes = (node_t *)calloc(2 * data_count, sizeof(node_t));
//	top_index = (int *) calloc(data_count, sizeof(int));

	
//}

void finalise() {
	free(top_index);
	free(frequency);
	free(nodes);
}


int add_node(int index, int weight) {
//	int 
	i = total_nodes++;
	
//Move existing nodes with larger weights to the right and
		while (i > 0 && nodes[i].weight > weight) {
		memcpy(&nodes[i + 1], &nodes[i], sizeof(node_t));
			if (nodes[i].index < 0)
				++leaf_index[-nodes[i].index];
		
			else
				++top_index[nodes[i].index];
				--i;
	}


//sorting the nodes so that they are well arranged
//===============================================
			++i;
			nodes[i].index = index;
			nodes[i].weight = weight;
			if (index < 0)
			leaf_index[-index] = i;
			else
			top_index[index] = i;
//========================================
	
	return i;
}

int write_header(FILE *f) {
	int j, byte = 0,
	size = sizeof(unsigned int) + 1 + data_count * (1 + sizeof(int));
	unsigned int weight;
	char *buffer = (char *) calloc(size, 1);
	if (buffer == NULL)	return -1;
	
	j = sizeof(int);
	while (j--)
		buffer[byte++] = (size_data >> (j << 3)) & 0xff;

	buffer[byte++] = (char) data_count;
	for (i = 1; i <= data_count; ++i) {
		weight = nodes[i].weight;
		buffer[byte++] =(char) (-nodes[i].index - 1);
		j = sizeof(int);
	while (j--)
	buffer[byte++] =
	(weight >> (j << 3)) & 0xff;
	}
	fwrite(buffer, 1, size, f);
	free(buffer);
	return 0;
}
//fundamental block of the tree
void add_leaves() {
	int count;
	
	for (i = 0; i < max_str; ++i) {
		count = freq[i];
		if (count > 0)
		add_node(-(i + 1), count);
	}
}

int write_bit(FILE *f, int bit) {
	if (buffer_bits == 256 << 3) {
	size_t bytes_written =
	fwrite(buffer, 1, 256, f);
	if (bytes_written < 256 && ferror(f))
	return -1;
	buffer_bits = 0;
	memset(buffer, 0, 256);
	}
	if (bit)
	buffer[buffer_bits >> 3] |= (0x1 << (7 - buffer_bits % 8));
	++buffer_bits;
	return 0;
}

void build_tree() {
	int a, b, index;
		while (free_index < total_nodes) {
			a = free_index++;
			b = free_index++;
			index = add_node(b/2, nodes[a].weight + nodes[b].weight);
			top_index[b/2] = index;
			}
}


int flush_buffer(FILE *f) {
	if (buffer_bits) {
		size_t bytes_written =
		fwrite(buffer, 1, (buffer_bits + 7) >> 3, f);
		if (bytes_written < 256 && ferror(f))
		return -1;
		buffer_bits = 0;
	}
	return 0;
}

//void encode_character(FILE *fout, int character) {
//	int node_index;
//	stack_top = 0;
//	node_index = leaf_index[character + 1];
//	while (node_index < total_nodes) {
//	stack[stack_top++] = node_index % 2;
//	node_index = top_index[(node_index + 1) / 2];
//	}
//	while (--stack_top > -1)
//	write_bit(fout, stack[stack_top]);
//}

int encode(FILE	*fin, FILE *fout) {
	int node_index;
//	Allocate space for coding tree and bit stack
	add_leaves();
	write_header(fout);
	build_tree();
	fseek(fin, 0, SEEK_SET);
//	int c;
	while ((i = fgetc(fin)) != EOF) {
		stack_top = 0;
		node_index = leaf_index[i + 1];
		while (node_index < total_nodes) {
			stack[stack_top++] = node_index % 2;
			node_index = top_index[(node_index + 1) / 2];
		}
		while (--stack_top > -1)
		write_bit(fout, stack[stack_top]);
	}
//		encode_character(fout, c);
	flush_buffer(fout);
	free(stack);
	return 0;
}


//information when program is run wrongly
void print_info(){
	printf("How to run: ./huffman [choose encode or decode] <input file> <output file>\n");
}

/*main program
====================================================*/
int main(int argc, char **argv){
	FILE *fin, *fout;
    
	if (argc != 4) {
	print_info();
	return 1;
	}
	
	fin = fopen(argv[2],"rb");
	if (fin == NULL) {
		perror("Failed to open input file\n");
		return -1;
		}
	
	readfile_freq(fin);
	leaf_index = frequency + max_str - 1;
	nodes = calloc(2 * data_count, sizeof(node_t));
	top_index = calloc(data_count, sizeof(int));		
	
	fout = fopen(argv[3], "wb");
	
	if (strcmp(argv[1], "-e") == 0)
		encode(fin, fout);
		
//	else if (strcmp(argv[1], "-d") == 0)
//		decode(fin, fout);
	
	else
		print_info();
	
	//frequency = (int *)	calloc(2 * num_alphabets, sizeof(int));
//	leaf_index = *freq) + max_str - 1;
	
	
	//allocate_tree();
	
	fclose(fin);
	fclose(fout);
	return 0;
}















//int read_bit(FILE *f) {
////to fill up buffer 
//
//
//	if (current_bit == bits_in_buffer) {
//		if (eof_input) return -1;
//		
//		else {
//		size_t bytes_read =
//		fread(buffer, 1, MAX_BUFFER_SIZE, f);
//		if (bytes_read < MAX_BUFFER_SIZE) {
//			if (feof(f))
//			eof_input = 1;
//		}
//		bits_in_buffer = bytes_read << 3;
//		current_bit = 0;
//		}
//}
//	if (bits_in_buffer == 0)
//	return -1;
//	int bit = (buffer[current_bit >> 3] >>
//	(7 - current_bit % 8)) & 0x1;
//	++current_bit;
//	return bit;
//}
