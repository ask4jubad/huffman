/*References: https://engineering.purdue.edu/ece264/17au/hw/HW13?alt=huffman
============================================================================
AUTHOR: JUBRIL GBOLAHAN ADIGUN
INSTITUTE: TALLINN UNIVESRSITY OF TECHNOLOGY
COURSE: SYSTEM PROGRAMMING
ASSIGNMENT: HUFFMAN ENCODER
FALL 2019
============================
*/

//==========================
//Including libraries

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CHAR 512
unsigned int char_freq[CHAR][CHAR];
unsigned int code_count[CHAR];
FILE* fout;
unsigned char bit_filler=0;
typedef unsigned char byte;

typedef struct node {
    unsigned int index;              /* only in a leaf */
    unsigned int weight;            /* frequecncy of character at the leaf */
    /* if left/right==NULL, it is a leaf. Otherwise it has two branches */
    struct node *left, *right;
	  
//    struct node *right;   /* not  in a leaf */
    int isLeaf_node;
} node;


struct charfreq { 
	unsigned int *arr_char; //= malloc(sizeof(int));
	unsigned int *arr_freq; //malloc(sizeof(int));
}; 
  
typedef struct charfreq Struct;

node* binary_tree;
node *nodes[CHAR] = {NULL};

struct node* create_node(unsigned int index, unsigned int weight); //takes eaxh character and makes a node from it
node* buildtree(int *freq); //builds tree nodes
node* create_tree(node* t1,node* t2); 
void traverse(struct node* root, int arr[], int top);
// void sort_nodes(int acc);


struct node* create_node(unsigned int index, unsigned int weight) {
        struct node* tree = (struct node*)malloc(sizeof(struct node));
        tree->index = index;
        tree->weight = weight;
        tree->isLeaf_node = 1;
        tree->left = NULL;
        tree->right = NULL;
        return(tree);
}

node* create_tree(node* t1, node* t2) {
    node *tree = malloc(sizeof(node));
    tree->index = 0;
    if(t1 && t2){
        (t1->weight <= t2->weight) ? 
			(tree->left = t1, tree->right = t2)
    	:
            (tree->left = t2, tree->right = t1);
        
        tree->weight = t1->weight + t2->weight;
        tree->isLeaf_node = 0;
    }
    else {
        tree->left = t1;
        tree->right = t2;
        tree->weight = t1->weight;
        tree->isLeaf_node = 0;
    }
    return tree;
}

// void sort_nodes(int acc) {
    
// }

node* buildtree(int *freq){
    int acc = 0, i, j;
    for(i =0; i < CHAR; i++) {
        if(freq[i] > 0){
            nodes[acc] = create_node(i,freq[i]);
            acc++;
        }
    }

//recursive call to compose the tree, creating dynamic 
    node* main_tree = (node*)malloc(sizeof(node));
    node* holder = (node*)malloc(sizeof(node));
    int is_first_time = 0;
    struct node* temp;
    for(i =0; i<acc; i++){
        for(j = i; j<acc; j++){
            if(nodes[i]->weight > nodes[j]-> weight){
                temp = nodes[i];
                nodes[i] = nodes[j];
                nodes[j] = temp;
            }
        }
    }

    // sort_nodes(acc);

    if (acc == 1){ //Character (CHAR - 2 ) to allow compression if only one tree exists
        main_tree = create_tree(nodes[0],NULL);
    }
    else {
        int remaining = CHAR;
        int i = 0;
        while(remaining > 1) {
            if(nodes[i]) {
                if(is_first_time == 0 && i <= CHAR - 2) { // This checks the first time build tree runs and main_tree giant_tree is null
                    main_tree = create_tree(nodes[i],nodes[i+1]);
                    i++;
                    is_first_time =1;
                }
                else {
                    holder = create_tree(main_tree,nodes[i]);
                    main_tree = holder;
                }
            }
            i++;
            remaining--;
        }
    }
    return main_tree;
}

void traverse(struct node* root, int arr[], int top) { 
    int i; 
    // Assign 0 to left edge and recur 
    if (root->left) { 
        arr[top] = 0;
        traverse(root->left, arr, top + 1); 
    } 
    
    //inorder traversal
    if (root->isLeaf_node == 1) {
        code_count[root->index] = top;
        for (i = 0; i < top; ++i){
            char_freq[root->index][i]=arr[i]%2;
        } 
     }
    // Assign 1 to right edge and recur
    if (root->right) { 
        arr[top] = 1; 
        traverse(root->right, arr, top + 1); 
    } 
}

Struct write_freq_table(unsigned int *freq) {
    int unique_char = 0, i,j, count=0;
    Struct result;
    for(i=0; i < CHAR; i++){
        if(freq[i] > 0) {
            unique_char++;
        }
    }
//    unique_char;
    fwrite(&unique_char,sizeof(int),1,fout);
    unsigned int arr_char[unique_char];
    unsigned int arr_freq[unique_char];
    for(j=0; j < CHAR; j++){
        if(freq[j] > 0) {
        	arr_char[count]=j;
        	arr_freq[count]=freq[j];
            fwrite(&j,sizeof(char),1,fout);
            fwrite(&freq[j],sizeof(int),1,fout);
            count++;
        }
    }
    result.arr_char = arr_char;
    result.arr_freq = arr_freq;
    
    return result;
}

void encode(FILE* finp, node* main_tree) {
    // rewind(finp);
    unsigned int n, i, ch;
    byte buf = 0, nbuf = 0;
    byte code[CHAR];
    while ((ch = fgetc(finp)) != EOF) {
        n = 0;
        for(i =0; i<code_count[ch]; i++){
            code[n] = char_freq[ch][i];
            n++;
        }
        for (i = 0; i < n; i++) {
            buf |= code[i] << nbuf;
            nbuf++;
            if (nbuf == 8) {
                fputc(buf, fout);
                nbuf = buf = 0;
            }
        }
    }
    fputc(buf, fout);
    bit_filler = 8 - nbuf;
    fwrite(&bit_filler,sizeof(char),1,fout);
}

void decode(FILE* finp) {
    size_t begin, end;
    unsigned unique_out=0, index=0, weight=0;
    unsigned int freq_out[CHAR]={0};
    fread(&unique_out, sizeof(int), 1, finp);
    int i;
    for (i = 0; i < unique_out; i++) {
        fread(&index, sizeof(char), 1, finp);
        fread(&weight, sizeof(int), 1, finp);
        freq_out[index] = weight;
    }
    binary_tree = buildtree(freq_out);

    begin = ftell(finp);
    fseek(finp, -1, SEEK_END);
    end = ftell(finp);
    fseek(finp, begin, SEEK_SET);
    int byte_count = end - begin;

    byte buf = 0;
    unsigned int ch;
    int bit, bit_count=8;
    struct node* node = binary_tree;
    while((ch = fgetc(finp)) != EOF) {
        if(byte_count == 1)
            break;
        else if(byte_count == 2)
            bit_count = 8 - bit_filler;
        buf = ch;
        for(i = 0; i<bit_count; i++){
            bit = buf & 1;
            buf >>= 1;
            if(bit == 0) {
                if(node ->left) {
                    node = node->left;
                }
                if(node->isLeaf_node == 1) {
                    fputc(node->index,fout);
                    node = binary_tree;
                }
            }
            else if (bit == 1) {
                if(node ->right) {
                    node = node->right;
                }
                if(node->isLeaf_node == 1) {
                    fputc(node->index,fout);
                    node = binary_tree;
               }
            }
        }
        byte_count--;
    }
}

//information when program is run wrongly
//==============================================
//void print_info(){
//	perror("How to run: ./huffman [choose encode or decode] <input file> <output file>\n");
//}


/*main program
====================================================*/
int main(int argc, char **argv){

    unsigned int freq[CHAR]={0};
    unsigned int size_data = 0;
    unsigned int c;

    FILE *finp;
	Struct outcome;
    /* code */
    // if (strcmp(argv[1], "-e") == 0)
	if (argc == 3) {
        finp = fopen(argv[1],"rb");
        fout = fopen(argv[2],"wb");

        if (finp == NULL) {
		perror("Failed to open input file: file may be corrupted of not availbale in the path/system \n");
		return 1;
		}
       
        else{
       // reference: https://stackoverflow.com/questions/30133210/check-if-file-is-empty-or-not-in-c/30133326
            fseek(finp, 0, SEEK_END);
            unsigned long len = (unsigned long) ftell(finp);
            
            if (len <= 0){
                // rewind(finp);
                fclose(finp);
                fclose(fout);
            }
            
            else{
            rewind(finp);
            while((c = fgetc(finp)) != EOF) { 
                freq[c]++;
                // c = fgetc(finp);
            }
            
            binary_tree = buildtree(freq);
            int arr[CHAR], top = 0;
            unsigned int *arr_char = malloc(sizeof(int));
            unsigned int *arr_freq= malloc(sizeof(int));
            
            outcome = write_freq_table(freq);
            
			arr_char = outcome.arr_char; 
			arr_freq = outcome.arr_freq;
			
            traverse(binary_tree, arr, top);
            encode(finp,binary_tree);
            fclose(finp);
            fclose(fout);
            }
        }
    }

    else if ((argc == 4) && (strcmp(argv[1], "-d") == 0)){
        finp = fopen(argv[2],"rb");
        fout = fopen(argv[3], "wb");
        
        fseek(finp, 0, SEEK_END);
        unsigned long len = (unsigned long) ftell(finp);
            
            if (len <= 0){
                rewind(finp);
            }

            else{
                rewind(finp);
                decode(finp);
            }

        fclose(finp);
        fclose(fout);
    }

    else {
       perror("How to run: ./huffman [choose encode or decode] <input file> <output file>\n");
        return 1;
    }
    
    return 0;

}
