/*References: 
https://cppgm.blogspot.com/2008/02/huffman-coding.html
https://engineering.purdue.edu/ece264/17au/hw/HW13?alt=huffman
https://stackoverflow.com/questions/30133210/check-if-file-is-empty-or-not-in-c/30133326
============================================================================
AUTHOR: JUBRIL GBOLAHAN ADIGUN
INSTITUTE: TALLINN UNIVESRSITY OF TECHNOLOGY
COURSE: SYSTEM PROGRAMMING
ASSIGNMENT: HUFFMAN ENCODER
FALL 2019
============================
*/

//==========================
//Including libraries

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CHAR 512
unsigned int char_freq[CHAR][CHAR];
unsigned int code_count[CHAR];
unsigned char ch_arr[CHAR] = {0};
unsigned int ch_freq[CHAR] = {0};
FILE* fout;
unsigned char bit_filler=0;
typedef unsigned char byte;
unsigned int size_data = 0;
int arr[CHAR], arr_index = 0;

typedef struct node {
    /* if left==0, it is a leaf. Otherwise it has two branches */
    struct node *left, *right;  // NULL values when it is a leaf */  
//    struct node ;   
    unsigned int index, weight;              /* leaf character occurence rate of the character */
//    unsigned int ;      
} node;

node* binary_tree;
node *nodes[CHAR] = {NULL};

struct node* create_node(unsigned int index, unsigned int weight); //takes each character and its and makes a node from it
node* buildtree(unsigned char* ch,unsigned int *freq); //builds tree nodes with each character
node* create_tree(node* t1,node* t2);  //recuursive call that takes two consecutive nodes and builds to tree
void traverse(struct node* root, int arr[], int arr_index); //traverse tree to generate codes until we reach leaf
void sort_arrays(); // to get 
void decode(FILE* finp); 
void encode(FILE* finp, node* root); //writes node information + compression into file

struct node* create_node(unsigned int index, unsigned int weight) {
        struct node* tree = (struct node*)malloc(sizeof(struct node));
        tree->index = index;
        tree->weight = weight;
        tree->left = NULL;
        tree->right = NULL;
        return(tree);
}

node* create_tree(node* t1, node* t2) {
    node *tree = malloc(sizeof(node));
    tree->index = 0;
    if(t1 && t2){
        if(t1->weight <= t2->weight){
            tree->left = t1;
            tree->right = t2;
        }
        else {
            tree->left = t2;
            tree->right = t1;
        }
        tree->weight = t1->weight + t2->weight;
    }
    else {
        tree->left = t1;
        tree->right = t2;
        tree->weight = t1->weight;
    }
    return tree;
}

node* buildtree(unsigned char *ch,unsigned int *freq){
    int acc = 0, i, j;
    for(i =0; i < CHAR; i++) {
        if(freq[i] > 0){
            nodes[acc] = create_node(ch[i],freq[i]);
            acc++;
        }
    }

    //recursive call to compose the tree, creating dynamic 
    node* main_tree = (node*)malloc(sizeof(node));
    node* holder = (node*)malloc(sizeof(node));
    int initial = 0;
    if (acc == 1){ //Character (CHAR - 2 ) to allow compression if only one tree exists
        main_tree = create_tree(nodes[0],NULL);
    }
    else {
        int remaining = CHAR;
        int i = 0;
        while(remaining > 1) {
            if(nodes[i]) {
                if(initial == 0 && i <= CHAR - 2) { // This checks the first time build tree runs and main_tree giant_tree is null
                    main_tree = create_tree(nodes[i],nodes[i+1]);
                    i++;
                    initial =1;
                }
                else {
                    holder = create_tree(main_tree,nodes[i]);
                    main_tree = holder;
                }
            }
            i++;
            remaining--;
        }
    }
    return main_tree;
}

void traverse(struct node* root, int arr[], int arr_index) { 
    int i; 
    // Assign 0 to left edge and recur 
    if (root->left) { 
        arr[arr_index] = 0;
        traverse(root->left, arr, arr_index + 1); 
        //fputc(0,fout);
    } 
    
    //inorder traversal
    if (!(root->left && root->right)) {
        code_count[root->index] = arr_index;
        //int count = arr_index;
        //int index = root->index;
        //int code;
        // fputc(-1,fout);
        // fputc(root->index,fout);
        //printf("%c - ", index);
        //fwrite(&count,sizeof(int),1,fout);
        //fwrite(&index,sizeof(char),1,fout);
        for (i = 0; i < arr_index; ++i){
            char_freq[root->index][i]=arr[i]%2;
            //code = arr[i];
            //printf("%d", code);
            //fwrite(&code,sizeof(char),1,fout);
        }
        //printf("\n");
     }
    // Assign 1 to right edge and recur
    if (root->right) { 
        arr[arr_index] = 1; 
        traverse(root->right, arr, arr_index + 1); 
        //fputc(1,fout);
    } 
}

void write_freq_table(unsigned char *arr,unsigned int *freq) {
    fwrite(&size_data,sizeof(int),1,fout);
    for(int j=0; j < CHAR; j++){
        if(freq[j] > 0) {
            fwrite(&arr[j],sizeof(char),1,fout);
            fwrite(&freq[j],sizeof(int),1,fout);
        }
    }
}

void encode(FILE* finp, node* main_tree) {
    // rewind(finp);
    unsigned int n, ch;
    byte buf = 0, nbuf = 0;
    byte code[CHAR];
    while ((ch = fgetc(finp)) != EOF) {
        n = 0;
        for(int i =0; i<code_count[ch]; i++){
            code[n] = char_freq[ch][i];
            n++;
        }
        for (int i = 0; i < n; i++) {
            buf |= code[i] << nbuf;
            nbuf++;
            if (nbuf == 8) {
                fputc(buf, fout);
                nbuf = buf = 0;
            }
        }
    }
    fputc(buf, fout);
    bit_filler = 8 - nbuf;
    //bit_filler = n - nbuf;
    fwrite(&bit_filler,sizeof(char),1,fout);
}

void decode(FILE* finp) {
    size_t begin, end;
    unsigned length=0, index=0, weight=0;
    fread(&length, sizeof(int), 1, finp);
    for (int i = 0; i < length; i++) {
        fread(&index, sizeof(char), 1, finp);
        fread(&weight, sizeof(int), 1, finp);
        ch_arr[i] = index;
        ch_freq[i] = weight;
    }

    // unsigned int length = 0;
    // unsigned int index;
    // unsigned int code;
    // unsigned int n = 0;
    // fread(&size_data, sizeof(int), 1, finp);
    // while(size_data > 0) {
    //     fread(&length, sizeof(int), 1, finp);
    //     fread(&index, sizeof(char), 1, finp);
    //     for(int i = 0; i < (length-1); i++){
    //         fread(&code, sizeof(char), 1, finp);
    //         char_freq[index][i] = code;
    //         n++;
    //     }
    //     code_count[index] = n;
    //     size_data--;
    //     n = 0;
    // }

    // struct node root;
    // fread(&root, sizeof(struct node)/* Just read one person */, 1, finp);
    // size_t size;
    // fread(&size, sizeof(size_t), 1, finp);
    // fread(&binary_tree, sizeof(node), size, finp);
    //sort_arrays();
    binary_tree = buildtree(ch_arr,ch_freq);

    begin = ftell(finp);
    //fpos_t pos;
    //fpos_t end_pos;
    //int *x = &pos;
    //int *y = &end_pos;
    //fgetpos (finp,&pos);
    fseek(finp, 0L, SEEK_END);
    //fgetpos (finp,&end_pos);
    end = ftell(finp);
    //fsetpos(finp, &pos);
    fseek(finp, begin, SEEK_SET);
    int byte_count = end - begin;
    byte buf = 0;
    unsigned int ch;
    int bit,bit_count=8;
    struct node* node = binary_tree;
    while(byte_count > 1) {
        ch = fgetc(finp);
        if(byte_count == 2){
            bit_count = 8 - bit_filler;
        }
        buf = ch;
        for(int i = 0; i<bit_count; i++){
            bit = buf & 1;
            buf >>= 1;
            if(bit == 0) {
                if(node ->left) {
                    node = node->left;
                }
                if(!(node->left && node->right)) {
                    fputc(node->index,fout);
                    node = binary_tree;
                }
            }
            else if (bit == 1) {
                if(node ->right) {
                    node = node->right;
                }
                if(!(node->left && node->right)) {
                    fputc(node->index,fout);
                    node = binary_tree;
               }
            }
        }
        byte_count--;
    }
}    

//information when program is run wrongly
//==============================================
// void print_info(){
// 	perror("How to run: ./huffman [choose encode or decode] <input file> <output file>\n");
// }

void sort_arrays(){
    unsigned temp_f, temp_c;
                for(int x = 0; x < size_data; x++){
                    for(int y = x; y < size_data; y++){
                        if(ch_freq[x] > ch_freq[y]) {
                            //sort frequency
                            temp_f = ch_freq[x];
                            ch_freq[x] = ch_freq[y];
                            ch_freq[y] = temp_f;
                            //sort character
                            temp_c = ch_arr[x]; 
                            ch_arr[x] = ch_arr[y];
                            ch_arr[y] = temp_c;
                        }  
                    }
                }
}
/*main program
====================================================*/
int main(int argc, char **argv){

    //unsigned int freq[CHAR]={0};
    unsigned int c;
    //Compress Debug
    // argc = 3;
    // argv[1] = "input";
    // argv[2] = "output";
    // //Decompress Debug
    // argc = 4;
    // argv[1] = "-d";
    // argv[2] = "x";
    // argv[3] = "y";
    FILE *finp;

    /* code */
    // if (strcmp(argv[1], "-e") == 0)
	if (argc == 3) {
        finp = fopen(argv[1],"rb");
        fout = fopen(argv[2],"wb");

        if (finp == NULL) {
		perror("Failed to open input file: file may be corrupted of not availbale in the path/system \n");
		return 1;
		}
       
        else{
       
            fseek(finp, 0, SEEK_END);
            unsigned long len = (unsigned long) ftell(finp);
            
            if (len <= 0){
                // rewind(finp);
                fclose(finp);
                fclose(fout);
            }
            
            else {
                rewind(finp);
                // while((c = fgetc(finp)) != EOF) { 
                //     freq[c]++;
                //     // c = fgetc(finp);
                // }
                int n = 0, exists = 0;
                while((c = getc(finp)) != EOF) {
                    for(int i=0; i < n; i++){
                        if(ch_arr[i] == c) {
                            exists = 1;
                            ch_freq[i]++;
                            break;
                        }
                    }
                    if(exists == 0){
                        size_data++;
                        ch_arr[n] = c;
                        ch_freq[n] ++;
                        n++;
                    }
                    exists = 0;
                }
                
                sort_arrays();
                binary_tree = buildtree(ch_arr,ch_freq);
                write_freq_table(ch_arr, ch_freq);
                traverse(binary_tree, arr, arr_index);
                rewind(finp);
                encode(finp,binary_tree);
                fclose(finp);
                fclose(fout);


                // finp = fopen("output","rb");
                // fout = fopen("ouputd", "wb");
                // unsigned int length = 0;
                // unsigned int index;
                // unsigned int code;
                // size_data = 0;
                // int c;
                // node* root;
                // fread(&size_data, sizeof(int), 1, finp);
                // while(size_data > 0) {
                //     c = fgetc(finp);
                //     if(c == -1){
                //         c = fgetc(finp);
                //         root = create_node(c,0);
                //     }
                //     else if(c == 0)
                //         root = root->left;
                //     else if(c == 1)
                //         root = root->right;  
                //     size_data--;
                // }
            }
        }
    }

    else if ((argc == 4) && (strcmp(argv[1], "-d") == 0)){
        finp = fopen(argv[2],"rb");
        fout = fopen(argv[3], "wb");
        
        fseek(finp, 0, SEEK_END);
        unsigned long len = (unsigned long) ftell(finp);
            
            if (len <= 0){
                rewind(finp);
            }

            else{
                rewind(finp);

                fseek(finp, 0L, SEEK_END);
                unsigned end = ftell(finp);
                fseek(finp, end-1, SEEK_SET);
                fread(&bit_filler, sizeof(char), 1, finp);
                rewind(finp);
                decode(finp);
            }

        fclose(finp);
        fclose(fout);
    }

    else {
        perror("How to run: ./huffman [choose encode or decode] <input file> <output file>\n");
        return 1;
    }
    
    return 0;

}
