#include <stdio.h>
#include <stdlib.h>

#define CHAR 256

typedef struct tree {
    /* if left==0, it is a leaf. Otherwise it has two branches */
    struct tree *left;    
    struct tree *right;   /* not  in a leaf */
    int index;              /* only in a leaf */
    int weight;            /* occurence rate of the tree */
    int isLeaf_node;
} tree;

tree *forest[CHAR] = {NULL};

struct tree* create_node(unsigned int index,unsigned int weight) {
        struct tree* tree = (struct tree*)malloc(sizeof(struct tree));
        tree->index = index;
        tree->weight = weight;
        tree->isLeaf_node = 1;
        tree->left = NULL;
        tree->right = NULL;
        return(tree);
}


tree* create_tree(tree* t1,tree* t2) {
    tree *new_tree = malloc(sizeof(tree));
    new_tree->index = 0;
    if(t1 && t2){
        if(t1->weight <= t2->weight){
            new_tree->left = t1;
            new_tree->right = t2;
        }
        else {
            new_tree->left = t2;
            new_tree->right = t1;
        }
        new_tree->weight = t1->weight + t2->weight;
        new_tree->isLeaf_node = 0;
    }
    else {
        new_tree->left = t1;
        new_tree->right = t2;
        new_tree->weight = t1->weight;
        new_tree->isLeaf_node = 0;
    }
    return new_tree;
}

void sort_forest(int treeCount) {
    struct tree* temp;
    int i, j;
    for(i =0; i<treeCount; i++){
        for(j = i; j<treeCount; j++){
            if(forest[i]->weight > forest[j]->weight){
                temp = forest[i];
                forest[i] = forest[j];
                forest[j] = temp;
            }
        }
    }
}

tree* buildtree(int *freq){
    int acc = 0, i;
    for(i =0; i <CHAR; i++) {
        if(freq[i] > 0){
            printf("%c - %d\n", i, freq[i]);
            forest[acc] = create_node(i,freq[i]);
            acc++;
        }
    }

    tree* giant_tree = (tree*)malloc(sizeof(tree));
    tree* cursor = (tree*)malloc(sizeof(tree));
    int is_first_time = 0;
    sort_forest(acc);
    if (acc == 1){ //Character (CHAR - 2 ) to allow compression if only one tree exists
        giant_tree = create_tree(forest[0],NULL);
    }
    else {
        int remaining = CHAR;
        int i = 0;
        while(remaining > 1) {
            if(forest[i] && forest[i]->weight > 0) {
                if(is_first_time == 0 && i <= CHAR - 2) { // Runs only first time when giant_tree is null
                    giant_tree = create_tree(forest[i],forest[i+1]);
                    i++;
                    is_first_time =1;
                }
                else {
                    cursor = create_tree(giant_tree,forest[i]);
                    giant_tree = cursor;
                }
            }
            i++;
            remaining--;
        }
    }
    return giant_tree;
}

void printCodes(struct tree* root, int arr[], int top) { 
  
    // Assign 0 to left edge and recur 
    if (root->left) { 
        arr[top] = 0; 
        printCodes(root->left, arr, top + 1); 
    } 
  
    // Assign 1 to right edge and recur
    if (root->right) { 
  
        arr[top] = 1; 
        printCodes(root->right, arr, top + 1); 
    } 
    
    if (root->isLeaf_node == 1) {
        printf("%c: ", root->index); 
            int i; 
    for (i = 0; i < top; ++i) 
          printf("%d", arr[i]); 
  
    printf("\n"); 
     }
}

int main () {
    tree* binary_tree;
    int freq[CHAR]={0};
    char c;
    FILE *fptr = fopen("input","rb");
    if (fptr == NULL) {
        printf("file not found");
        return 1;
    }
    else {
       while((c = getc(fptr)) != EOF) { 
           freq[c] ++;
        }
        binary_tree = buildtree(freq);
    }
    int arr[CHAR], top = 0;
    printCodes(binary_tree, arr, top);
    return 0;
}
